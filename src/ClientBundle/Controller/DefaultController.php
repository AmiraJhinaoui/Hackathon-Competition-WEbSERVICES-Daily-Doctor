<?php

namespace ClientBundle\Controller;

use ClientBundle\Entity\EtatSanteUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\User;

use ClientBundle\Entity\Medicament;
use ClientBundle\Entity\Cabinet;
use ClientBundle\Entity\Symptome;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ClientBundle:Default:index.html.twig');
    }

                  /////////////////////////////LOGIN////////////////////////////
    ///
    public function loginAction(Request $request)
    {
        $result = array();

        $repository = $this->getDoctrine()
            ->getRepository(User::class);
        $query = $repository->createQueryBuilder('p')
            ->where('p.passwordPlain =  :password')
            ->andwhere('p.username = :username')
            ->setParameter('username', $request->get('username'))
            ->setParameter('password', $request->get('password'))
            ->getQuery();
        $user = $query->getResult();

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);

        if ($user == null) {
            $result['status'] = false;
            $result['content'] = "Erreur: username ou mot de passe incorrecte";
        } else {
            $result['status'] = true;
            $result['content'] = $formatted;
        }
        return new JsonResponse($result);

    }
           /////////////////////// SIGN UP///////////////////////////

    public function signUpAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = new User();
        $user->setNom($request->get('nom'));
        $user->setPrenom($request->get('prenom'));
        $user->setSexe($request->get('sexe'));
        $user->setTelephone($request->get('telephone'));

        $time = strtotime($request->get('date_naissance'));
        $newformat = date('Y-m-d', $time);
        $user->setDateNaissance(new \DateTime($newformat));

        $user->setMail($request->get('mail'));
        $user->setPassword($request->get('password'));
        $medicament = $em->getRepository('ClientBundle:Medicament')->find($request->get('medicament'));
        $user->setIdMedicament($medicament);
        $user->setLongitude($request->get('longitude'));
        $user->setLatitude($request->get('latitude'));
        $user->setChronique($request->get('chronique'));
        $user->setDescription($request->get('description'));
        $user->setAdresse($request->get('adresse'));

        /*UpLOAD IMAGE*/

        $filePh = $request->files->get('phot');
        $imgExtension = $request->files->get('photo')->guessExtension();
        $imgNameWithoutSpace = str_replace(' ', '', $request->request->get('nom'));
        $imgName = $imgNameWithoutSpace . "." . $imgExtension;
        $filePh->move($this->getParameter('user_directory'), $imgName);
        $user->setPhoto("client/images/user/" . $imgName);


        $user->persist($user);

        $result = array();
        try {
            $em->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($user);
            $result['status'] = true;
            $result['content'] = $formatted;
        } catch (\Exception $exp) {
            $result['status'] = false;
            $result['content'] = "Error :Try AGAIN";
        }


        return new JsonResponse($result);
    }


                                      ////////////Changer profil///////////////////

    public function changerProfilAction(Request $request)
    {

        if ($request->isMethod("post")) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('ClientBundle:User')->find($this->getUser()->getId());
            $user->setNom($request->get('nom'));
            $user->setPrenom($request->get('prenom'));

            $time = strtotime($request->get('date_naissance'));
            $newformat = date('Y-m-d', $time);
            $user->setDateNaissance(new \DateTime($newformat));

            $user->setTelephone($request->get('telephone'));
            $user->setMail($request->get('mail'));
            $user->setSexe($request->get('sexe'));
            $user->setAdresse($request->get('adresse'));
            $user->setChronique($request->get('chronique'));
            $medicament = $em->getRepository('ClientBundle:Medicament')->find($request->get('medicament'));
            $user->setUser($user);


            $result = array();
            try {
                $em->merge($user);
                $em->flush();
                $serializer = new Serializer([new ObjectNormalizer()]);
                $formatted = $serializer->normalize($user);
                $result['status'] = true;
                $result['content'] = $formatted;
            } catch (\Exception $exp) {
                $result['status'] = false;
                $result['content'] = "Erreur: matricule deja existe";
            }


            return new JsonResponse($result);
        }
    }



                             //////////////ajouter un etat de sante///////////////

    public function ajouterEtatAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $etat = new EtatSanteUser();
        $etat->setNiveau($request->get('niveau'));
        $newformat = date('H:i:s \O\n Y-m-d');
        $etat->setDate(new \DateTime($newformat));


        $user = $em->getRepository('ClientBundle:User')->find($request->get('user'));
        $etat->setIdUser($user);

        $symptome = $em->getRepository('ClientBundle:Symptome')->find($request->get('symptome'));
        $etat->setIdSymptome($symptome);

        $user->persist($user);

        $result = array();
        try {
            $em->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($etat);
            $result['status'] = true;
            $result['content'] = $formatted;
        } catch (\Exception $exp) {
            $result['status'] = false;
            $result['content'] = "Error :Try AGAIN";
        }


        return new JsonResponse($result);

    }


    public function analyserEtatAction(Request $request){





    }


    public function alerterEtat(Request $request)
    {

    }

    public function afficherCabinetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cabinet = $em->getRepository('ClientBundle:Cabinet')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($cabinet);
        return new JsonResponse($formatted);
    }
    public function afficherBySpecialyAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cabinet = $em->getRepository('ClientBundle:Cabinet')->findBySpecialite($this->getSpecialite());
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($cabinet);
        return new JsonResponse($formatted);
    }


    public function afficherMedicamentAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $medicament = $em->getRepository('ClientBundle:Medicament')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($medicament);
        return new JsonResponse($formatted);
    }


    /////////////Afficher les etats par date//////////

    public function afficherEtatByDateAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $etat = $em->getRepository('ClientBundle:EtatSanteUser')->findByDate();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($etat);
        return new JsonResponse($formatted);
    }


    //////////Recherche Medicament//////////////

    public function chercherMedicAction(Request $request){

        $nomMedic = $request->get('search_nomMedic');
        $indicationMedic = $request->get('search_indicationMedic');
        $repository = $this->getDoctrine()
            ->getRepository(Medicament::class);
        $query = $repository->createQueryBuilder('p')
            ->where('p.nomMedicament LIKE  :nomMedic')
            ->andwhere('p.indicationMedicament LIKE :indicationMedic')

            ->setParameter('nomMedicament', '%'.$nomMedic.'%')
            ->setParameter('indicationMedicament', '%'.$indicationMedic.'%')


            ->getQuery();
        $resultMedicaments = $query->getResult();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $result = $serializer->normalize($resultMedicaments);
        return new JsonResponse($result);
    }



}
