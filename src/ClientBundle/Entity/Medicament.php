<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Medicament
 *
 * @ORM\Table(name="medicament")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\MedicamentRepository")
 */
class Medicament
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomMedicament", type="string", length=255)
     */
    private $nomMedicament;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="time")
     */
    private $heure;

    /**
     * @var string
     *
     * @ORM\Column(name="indicationMedicament", type="string", length=255)
     */
    private $indicationMedicament;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomMedicament
     *
     * @param string $nomMedicament
     *
     * @return Medicament
     */
    public function setNomMedicament($nomMedicament)
    {
        $this->nomMedicament = $nomMedicament;

        return $this;
    }

    /**
     * Get nomMedicament
     *
     * @return string
     */
    public function getNomMedicament()
    {
        return $this->nomMedicament;
    }

    /**
     * Set heure
     *
     * @param \DateTime $heure
     *
     * @return Medicament
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    /**
     * Get heure
     *
     * @return \DateTime
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * Set indicationMedicament
     *
     * @param string $indicationMedicament
     *
     * @return Medicament
     */
    public function setIndicationMedicament($indicationMedicament)
    {
        $this->indicationMedicament = $indicationMedicament;

        return $this;
    }

    /**
     * Get indicationMedicament
     *
     * @return string
     */
    public function getIndicationMedicament()
    {
        return $this->indicationMedicament;
    }
}

