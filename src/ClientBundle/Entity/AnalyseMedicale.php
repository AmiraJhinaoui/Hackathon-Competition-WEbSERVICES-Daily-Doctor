<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnalyseMedicale
 *
 * @ORM\Table(name="analyse_medicale")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\AnalyseMedicaleRepository")
 */
class AnalyseMedicale
{


    /**
     * @var \ClientBundle\Entity\Maladie
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Maladie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_maladie", referencedColumnName="id")
     * })
     */
    private $idMaladie;

    /**
     * @var \ClientBundle\Entity\Symptome
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Symptome")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_symptome", referencedColumnName="id")
     * })
     */

    private $idSymptome;

    /**
     * @var string
     *
     * @ORM\Column(name="nomAnalyse", type="string", length=255)
     */
    private $nomAnalyse;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="string", length=255, nullable=true)
     */
    private $description;




    /**
     * Set idMaladie
     *
     * @param integer $idMaladie
     *
     * @return AnalyseMedicale
     */
    public function setIdMaladie($idMaladie)
    {
        $this->idMaladie = $idMaladie;

        return $this;
    }



    /**
     * Set idSymptome
     *
     * @param integer $idSymptome
     *
     * @return AnalyseMedicale
     */
    public function setIdSymptome($idSymptome)
    {
        $this->idSymptome = $idSymptome;

        return $this;
    }

    /**
     * Get idSymptome
     *
     * @return int
     */
    public function getIdSymptome()
    {
        return $this->idSymptome;
    }

    /**
     * Set nomAnalyse
     *
     * @param string $nomAnalyse
     *
     * @return AnalyseMedicale
     */
    public function setNomAnalyse($nomAnalyse)
    {
        $this->nomAnalyse = $nomAnalyse;

        return $this;
    }

    /**
     * Get nomAnalyse
     *
     * @return string
     */
    public function getNomAnalyse()
    {
        return $this->nomAnalyse;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AnalyseMedicale
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

