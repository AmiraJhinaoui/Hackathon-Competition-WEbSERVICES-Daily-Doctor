<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Maladie
 *
 * @ORM\Table(name="maladie")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\MaladieRepository")
 */
class Maladie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomMaladie", type="string", length=255)
     */
    private $nomMaladie;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomMaladie
     *
     * @param string $nomMaladie
     *
     * @return Maladie
     */
    public function setNomMaladie($nomMaladie)
    {
        $this->nomMaladie = $nomMaladie;

        return $this;
    }

    /**
     * Get nomMaladie
     *
     * @return string
     */
    public function getNomMaladie()
    {
        return $this->nomMaladie;
    }
}

