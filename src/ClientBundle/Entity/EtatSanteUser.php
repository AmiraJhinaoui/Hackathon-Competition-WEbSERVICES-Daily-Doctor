<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtatSanteUser
 *
 * @ORM\Table(name="etat_sante_user")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\EtatSanteUserRepository")
 */
class EtatSanteUser
{


    /**
     * @var \ClientBundle\Entity\User
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var \ClientBundle\Entity\Symptome
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Symptome")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_symptone", referencedColumnName="id")
     * })
     */
    private $idSymptome;

    /**
     * @var int
     *
     * @ORM\Column(name="niveau", type="integer")
     */
    private $niveau;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEtat", type="datetime")
     */
    private $dateEtat;




    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return EtatSanteUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idSymptome
     *
     * @param integer $idSymptome
     *
     * @return EtatSanteUser
     */
    public function setIdSymptome($idSymptome)
    {
        $this->idSymptome = $idSymptome;

        return $this;
    }

    /**
     * Get idSymptome
     *
     * @return int
     */
    public function getIdSymptome()
    {
        return $this->idSymptome;
    }

    /**
     * Set niveau
     *
     * @param integer $niveau
     *
     * @return EtatSanteUser
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return int
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set dateEtat
     *
     * @param \DateTime $dateEtat
     *
     * @return EtatSanteUser
     */
    public function setDateEtat($dateEtat)
    {
        $this->dateEtat = $dateEtat;

        return $this;
    }

    /**
     * Get dateEtat
     *
     * @return \DateTime
     */
    public function getDateEtat()
    {
        return $this->dateEtat;
    }
}

