<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cabinet
 *
 * @ORM\Table(name="cabinet")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\CabinetRepository")
 */
class Cabinet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomCabinet", type="string", length=255)
     */
    private $nomCabinet;

    /**
     * @var string
     *
     * @ORM\Column(name="nomDoctor", type="string", length=255)
     */
    private $nomDoctor;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomDoctor", type="string", length=255)
     */
    private $prenomDoctor;

    /**
     * @var string
     *
     * @ORM\Column(name="Specialite", type="string", length=255)
     */
    private $specialite;

    /**
     * @var int
     *
     * @ORM\Column(name="telephoneDoctor", type="integer")
     */
    private $telephoneDoctor;

    /**
     * @var float
     *
     * @ORM\Column(name="longCabinet", type="float")
     */
    private $longCabinet;

    /**
     * @var float
     *
     * @ORM\Column(name="latDoctor", type="float")
     */
    private $latDoctor;

    /**
     * @var string
     *
     * @ORM\Column(name="adresseDoctor", type="string", length=255)
     */
    private $adresseDoctor;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomCabinet
     *
     * @param string $nomCabinet
     *
     * @return Cabinet
     */
    public function setNomCabinet($nomCabinet)
    {
        $this->nomCabinet = $nomCabinet;

        return $this;
    }

    /**
     * Get nomCabinet
     *
     * @return string
     */
    public function getNomCabinet()
    {
        return $this->nomCabinet;
    }

    /**
     * Set nomDoctor
     *
     * @param string $nomDoctor
     *
     * @return Cabinet
     */
    public function setNomDoctor($nomDoctor)
    {
        $this->nomDoctor = $nomDoctor;

        return $this;
    }

    /**
     * Get nomDoctor
     *
     * @return string
     */
    public function getNomDoctor()
    {
        return $this->nomDoctor;
    }

    /**
     * Set prenomDoctor
     *
     * @param string $prenomDoctor
     *
     * @return Cabinet
     */
    public function setPrenomDoctor($prenomDoctor)
    {
        $this->prenomDoctor = $prenomDoctor;

        return $this;
    }

    /**
     * Get prenomDoctor
     *
     * @return string
     */
    public function getPrenomDoctor()
    {
        return $this->prenomDoctor;
    }

    /**
     * Set specialite
     *
     * @param string $specialite
     *
     * @return Cabinet
     */
    public function setSpecialite($specialite)
    {
        $this->specialite = $specialite;

        return $this;
    }

    /**
     * Get specialite
     *
     * @return string
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * Set telephoneDoctor
     *
     * @param integer $telephoneDoctor
     *
     * @return Cabinet
     */
    public function setTelephoneDoctor($telephoneDoctor)
    {
        $this->telephoneDoctor = $telephoneDoctor;

        return $this;
    }

    /**
     * Get telephoneDoctor
     *
     * @return int
     */
    public function getTelephoneDoctor()
    {
        return $this->telephoneDoctor;
    }

    /**
     * Set longCabinet
     *
     * @param float $longCabinet
     *
     * @return Cabinet
     */
    public function setLongCabinet($longCabinet)
    {
        $this->longCabinet = $longCabinet;

        return $this;
    }

    /**
     * Get longCabinet
     *
     * @return float
     */
    public function getLongCabinet()
    {
        return $this->longCabinet;
    }

    /**
     * Set latDoctor
     *
     * @param float $latDoctor
     *
     * @return Cabinet
     */
    public function setLatDoctor($latDoctor)
    {
        $this->latDoctor = $latDoctor;

        return $this;
    }

    /**
     * Get latDoctor
     *
     * @return float
     */
    public function getLatDoctor()
    {
        return $this->latDoctor;
    }

    /**
     * Set adresseDoctor
     *
     * @param string $adresseDoctor
     *
     * @return Cabinet
     */
    public function setAdresseDoctor($adresseDoctor)
    {
        $this->adresseDoctor = $adresseDoctor;

        return $this;
    }

    /**
     * Get adresseDoctor
     *
     * @return string
     */
    public function getAdresseDoctor()
    {
        return $this->adresseDoctor;
    }
}

