<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Symptome
 *
 * @ORM\Table(name="symptome")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\SymptomeRepository")
 */
class Symptome
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomSymptome", type="string", length=255)
     */
    private $nomSymptome;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomSymptome
     *
     * @param string $nomSymptome
     *
     * @return Symptome
     */
    public function setNomSymptome($nomSymptome)
    {
        $this->nomSymptome = $nomSymptome;

        return $this;
    }

    /**
     * Get nomSymptome
     *
     * @return string
     */
    public function getNomSymptome()
    {
        return $this->nomSymptome;
    }
}

